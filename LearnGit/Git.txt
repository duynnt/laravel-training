1.Repository là gì? Phân biệt Remote repository và Local repository?
 - Repository như là kho chứa tất cả dữ liệu
 - Remote repository gọi là kho lưu trữ và nó ở trên server
 - Local repository được đặt ở máy chúng ta
2.Clone là gì? Để thực hiện clone cần yếu tố gì và sử dụng câu lệnh gì? 
 - Clone là bản sao của project nào đó giúp chúng ta lấy dữ liệu từ git về máy.
 - Câu lệnh: "git clone <địa chỉ của project>"
3.Phân biệt các trạng thái của file sau: 
 - Committed 
  + Các file được đánh để commit.
 - Unmodified
  + Khi các file được đánh dấu và sau khi commit thì sẽ không thay đổi.
 - Untracked 
  + File chưa được thêm vào git.
 - Unstaged hoặc Modified 
  + Các file bị thay đổi.
 - Staged 
  + Khi 1 file thay đổi sẽ được đánh dấu lại để commit.
 Và sử dụng lệnh gì để kiểm tra trạng thái file hiện tại? 
  - Sử dụng câu lệnh “git status” or "git status -s" để xem trạng thái 
4. Cách để thêm(add) một file vào trạng thái Staged là gì? Cách hủy bỏ chức năng 
add vừa rồi là gì? (undo add) 
  - Sử dụng lệnh “git add <tên file >” hoặc “git add . “ để thực hiện chức năng add
  - Sử dụng lệnh “git reset <tên file>” hoặc “git restore <tên file>"để hủy bỏ chức năng add
5. Lệnh git status -s để xem trạng thái của file dưới dạng ngắn gọn. Giải thích ý 
nghĩa của các ký hiệu trạng thái trước tên file: 
 M a.txt 
  - File đã tồn tại, sửa đổi và đã add lại vào git.
 M b.txt 
  - Nghĩa là khi 1 file đã được thay đổi về nội dung trong đó nhưng chưa add lại.
 MM c.txt 
  - Là những file đã có nhưng thay đổi về nội dung và chưa add lại git.
 A d.txt 
  - Những file đã thêm mới.
 AM e.txt 
  - Những file đã thêm mới và file đó thay đổi về nội dung chưa add lại vào git.
 ?? f.txt 
  - Khi tạo mới 1 file mà chưa add vào git
6. Giải thích ý nghĩa của lệnh git diff và git diff –staged
 - git diff thể hiện sự thay đổi của file đang làm việc
 - git diff –staged thể hiện sự thay đổi của folder đang làm việc
7. Commit là gì? Câu lệnh đơn giản để thực hiện Commit là gì?
 - Ghi lại hoặc thay đổi file hay thư mục, nó sẽ nghi lại sự thay đổi của lần trước.
 - Câu lệnh “git commit -m “text””
 Giải thích ý nghĩa của lệnh sau: 
  git rm -f file.txt 
   - Dùng để loại bỏ 1 file trong thư mục file.txt hiện hành
  git rm -cached file.txt (biết rằng file.txt không phải là trạng thái untracked) 
   - Dùng để loại bỏ file trong thư mục ra khỏi git
. Mục đích Ignore là gì? Nêu cách Ignore file hoặc folder? 
- Ignore là liệt kê những file mà mình không mong muốn cho vào git hay git sẽ bỏ qua những file đó.
- Cách Ignore file 
Khi add 1 file mới vào git, git sẽ kiểm tra danh sách những file sẽ bỏ qua trong file .gitignore và không add chúng vào git và files không có trong git cache nữa thì git nó mới bỏ qua, chứ files mà nằm trong git cache thì .gitignore sẽ không có tác dụng.
9. Giải thích ý nghĩa của lệnh sau: 
  git log 
   - Là xem lại lịch sử commit nhằm giám sát được sự thay đổi của project.
  git log --oneline 
   - Cho biết lịch sử các nhánh giám sát được tất cả sự thây đổi project.
  git show
   - Hiển thị nhiều loại đối tượng như sự thay đổi của file kể cả commit 
10. Giải thích ý nghĩa của lệnh sau: 
  git remote add origin <url> 
   - Để thêm một remote repo vào local repo
  git remote -v 
   - Xem remote mà bạn đã thêm một cách chi tiết
11. Branch là gì? Mục đích sử dụng Branch là gì?
   - Mục đích phân nhánh sẽ không ảnh hưởng đến branch khác nên có thể tiến hành nhiều thay đổi đồng thời trong cùng 1 repository. Branch đã phân nhánh có thể chỉnh sửa tổng hợp lại thành 1 branch bằng việc hợp lại (merge) với branch khác.
12. Nêu cách thực hiện: 
  Liệt kê các branch hiện tại 
   - Thực hiện câu lệnh “git branch ”
  Tạo mới một branch
   - Thực hiện câu lệnh “git checkout -b <tên>” 
  Chuyển tới một branch để làm việc 
   - Thực hiện câu lệnh “git checkout <tên>”
  Xóa branch 
   - Thực hiện câu lệnh “git branch -d <tên>”
  Đổi tên branch 
   - Thực hiện câu lệnh “git branch -m <tên cần đổi>”
13. Merge là gì? Nêu cách thực hiện 
  - Merge khi muốn hợp nhất 2 branch lại với nhau
  - Cách thực hiện vào git click chọn merge request -> chọn branch muốn merge và chọn branch được merge sau đó click merger
14. Rebase là gì? Nêu cách thực hiện? Sự khác biệt giữa Rebase và Merge ?
  - Rebase cũng giống như merge và nó sẽ merge hai branch lại với nhau và nó sẽ chọn commit mới nhất.
  - Cách thực hiện:
    =>git checkout dev
    =>git pull origin dev
    =>git checkout branch_minh
    =>git rebase dev 
   (
    =>(nếu báo chưa commit một số file cần bỏ)
    =>git checkout .
    =>git status (kiểm tra lại)
    =>git rabase dev
   )
    =>git pull
    =>git push origin branch_minh
    Khi ở màn hình lựa => :wp=>enter
    Hoặc có thể dùng trực tiếp “git rebase”
  - Sự khác biệt
- Rebase sẽ duyệt qua từng commit của nhánh đang làm việc và thực hiện thay đổi và trong qua trình làm việc nó có sinh ra biến tạm.
- Merge là kết quả của khi trộn giữa 3 commit, đó là commit cha chung và 2 commit mới nhất của 2 nhánh, kết quả sẽ tạo ra một commit chứa kết quả của trộn 2 file.
15. Conflict là gì? Conflict xảy ra trong những trường hợp nào và nêu cách giải 
quyết? 
  - Conflict là những xung đột xảy ra khi merge
  - Conflict xảy ra trong các trường hợp sau.
    + Đầu tiên, dòng "<<<<<<< HEAD" là nội dung tồn tại trong nhánh master hiện tại mà tham chiếu HEAD đang trỏ tới.
    + Thứ hai, tất cả nội dung phía sau dòng "=======" là "trung tâm" của cuộc xung đột. Thể hiện nội dung xung đột. 
    + Thứ ba, tất cả nội dung phía sau dòng ">>>>>>> xx" là nội dung có trong nhánh xx chuẩn bị hợp nhất vào master
  - Cách giải quyết: tùy theo lỗi mà chỉnh sửa nội dung trên file mà nó dẫn tới xung đột, sau đó thực hiện add, commit cho file đó.
16. Bạn có thể làm việc trên nhánh master không? Vì sao? Nếu bạn được giao làm một tính năng, bạn sẽ sử dụng quy trình làm việc của git như thế nào? 
  - Có thể làm việc trên nhánh master nhưng hạn chế bởi vì khi 1 project thì thường là 1 team mà làm chung vào nhánh master sẽ khó khăn trong việc kiểm soát cũng như về dữ liệu.
  - Nếu như được giao làm 1 tính năng:
    + Clone project về 
    + Tạo branch mới và làm việc trên branch đó
    + Thực hiện công việc add lại, commit và push lại sau mỗi công đoạn hoặc có thể khi kết thúc nhiệm vụ.
17. So sánh lệnh Fetch và Pull? 
  - Câu lệnh git pull sẽ tải về dữ liệu từ một branch từ remote server và sau đó merge các thay đổi từ remote này vào repository dưới local.
  - git fetch sẽ tải về (fetch) dữ liệu của toàn bộ các branch trên URL
18. Push là gì? 
  - Là đẩy mới một branch lên remote
19. Merge request là gì? Sử dụng khi nào? 
  - Là tạo yêu cầu hợp nhất khi có thay đổi về nội dung và muốn hợp nhất với project sẵn có ở remote (nó tương tự giống với merge)
  - Sử dụng khi thay đổi về nội dung của project cần hợp nhất lại với nhau để hoàn thiện về project.
20. Revert là gì? Nêu cách hủy bỏ (revert) những gì đang thay đổi của một file, toàn bộ dự án? 
  - Là lệnh backup lại các commit (trở lại các commit)
  - Git revert “id commit” sẽ quay trở lại khi commit file đó.
21. Giả sử có các commit theo thứ tự thời gian từ mới đến cũ: 
commit_Id_4 commit_Id_3 commit_Id_2 commit_Id_1 
  - Làm thế nào để revert tới commit_Id_2 ? 
    +Git revert commit_Id_2
22. Phân biệt 
  - git reset –hard
    + Về reset commit cuối cùng nhưng sẽ xóa hoàn toàn commit đó không lưu vào trong Staged
  - git reset –soft
   + Đưa head về vị trí cụ thể và giữ nguyên các thay đổi đã được git add tại vị trí đó trong Staging Area.(nội dung bên trong không bị xóa hẳn có thể thực hiện commit khác)
  - git reset --mix
   + Thiết lập lại vị trí head
  - git reset
   + Đặt lại trạng thái đầu nó không xóa bất kỳ dữ liệu.
23. Giải thích ý nghĩa của lệnh sau: 
  - git clean -f
   + Xóa untracked files (Những file mới tạo và nhưng file được tạo tự động bởi chương trình)
  - git clean -f -d
   + Xóa untracked directories (Thư mục mới tạo và thư mục được tạo tự động bởi chương trình)
