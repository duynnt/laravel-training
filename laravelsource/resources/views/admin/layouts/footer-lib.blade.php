<!-- jQuery -->
<script src="{{ asset('libs/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('libs/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('libs/chart.js/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('libs/sparklines/sparkline.js') }}"></script>
<!-- JQVMap -->
<script src="{{ asset('libs/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('libs/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('libs/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('libs/moment/moment.min.js') }}"></script>
<script src="{{ asset('libs/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('libs/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('libs/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('libs/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('libs/adminlte/dist/js/adminlte.js') }}"></script>
<!-- AdminLTE for demo purposes -->
{{-- <script src="{{ asset('libs/adminlte/dist/js/demo.js') }}"></script> --}}
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('libs/adminlte/dist/js/pages/dashboard.js') }}"></script>
<!-- Toastr -->
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<!-- datatable -->
<script src="//cdn.datatables.net/1.11.2/js/jquery.dataTables.min.js"></script>
<!-- bootbox -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<!-- select2 -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript">
  @if (session('success'))
    toastr.success("{{ session('success') }}", "Thông báo");
  @endif
  $.extend(true, $.fn.dataTable.defaults,{
    "lengthMenu": [10, 50, 100, 500, 1000, 5000],
    "language": {
        "sLengthMenu": "Hiển thị _MENU_ dòng trên 1 trang",
        "sZeroRecords": "Không tìm thấy dữ liệu",
        "info": "Hiển thị trang _PAGE_ trong tổng số _PAGES_ trang",
        "sInfoEmpty": "Không có dữ liệu nào",
        "sInfoFiltered": "(được lọc từ tổng sô _MAX_ trong dữ liệu)",
        "sSearch": "Tìm kiếm:",
        "oPaginate": {
            "sNext": "Sau",
            "sPrevious": "Trước"
            },
        }
  })

  // active menu
  $(function(){
      var url = window.location.href; 
      $(".nav-sidebar a").each(function() {
          if(url == (this.href)) { 
              $(this).addClass("active");
          }
      });
  });
</script>
<!-- Custom -->
@stack('scripts')