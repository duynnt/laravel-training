@extends('admin.layouts.main',['title' => 'Products'])
@push('styles')
{{-- <link rel="stylesheet" href="style.css"> --}}
<style style="text/css">
  /* .content-wrapper{
    background-color: red;
  } */
</style>
@endpush
@section('content')
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="mb-2 row">
          <div class="col-sm-6">
            <h1 class="m-0">Products</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if ($products)
          <table class="w-full whitespace-nowrap">
            <thead>
              <tr class="text-left font-weight-bold">
                <td class="px-4 py-2 border">STT</td>
                <td class="px-4 py-2 border">Name</td>
                <td class="px-4 py-2 border">Price</td>
                <td class="px-4 py-2 border">Description</td>
                <td class="px-4 py-2 border">Image</td>
              </tr>
            </thead>
            <tbody>
              @foreach ($products as $key => $product)
                <tr class=" hover:bg-gray-200">
                  <td class="px-4 py-2 border">{{ $key+1 }}</td>
                  <td class="px-4 py-2 border">{{ $product['name'] }}</td>
                  <td class="px-4 py-2 border">{{ $product['price'] }}</td>
                  <td class="px-4 py-2 border">{{ $product['description'] }}</td>
                  <td class="px-4 py-2 border">{{ $product['image'] }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        @endif
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
@push('scripts')
<script type="text/javascript">
//  alert('s');
</script>
@endpush