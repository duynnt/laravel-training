@extends('admin.layouts.main',['title' => 'Task'])
@push('styles')
    {{-- <link rel="stylesheet" href="style.css"> --}}
    <style style="text/css">
        .wj-flexgrid {
            max-height: 350px;
        }

        body {
            margin-bottom: 20pt;
        }

        @import url("http://bootswatch.com/simplex/bootstrap.min.css");

        table .collapse.in {
            display: table-row;
        }
		.readmore{
			cursor: pointer;
		}
		.readmore i{
			font-size: 20px;
		}
		.collapse td{
			background: #fff;
		}
    </style>
@endpush
@section('content')
    <table class="table table-responsive table-hover">
        <thead>
            <tr>
                <th></th>
                <th>Name</th>
                <th>Price</th>
                <th>Description</th>
                <th>Image</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $key => $product)
                <tr class="clickable" data-toggle="collapse" id="row{{ $key }}" data-target=".row{{ $key }}">
                    @if (count($product->colors) > 0 )
						<td class="readmore"><i class="fas fa-angle-down"></i></td>
					@else
						<td></td>
					@endif
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->price }}</td>
                    <td>{{ $product->description }}</td>
                    <td>{{ $product->image }}</td>
                </tr>
                @foreach ($product->colors as $color)
                    <tr class="collapse row{{ $key }}">
                        <td></td>
                        <td>{{ $color->color }}</td>
                        <td>{{ $color->hex }}</td>
                    </tr>
                @endforeach

            @endforeach
        </tbody>
    </table>
@endsection
@push('scripts')
<script type="text/javascript">
	var x = 0;
	$('.clickable').click(function(){
		let element = $(this).children().children();
		if (getRotationDegrees(element) <= 0){
			element.css({'transform':'rotate(-180deg)','transition':'.6s'});	
		}else{
			element.css({'transform':'rotate(0deg)','transition':'.6s'});	
		}
	});
	function getRotationDegrees(obj) {
		var matrix = obj.css("-webkit-transform") ||
		obj.css("-moz-transform")    ||
		obj.css("-ms-transform")     ||
		obj.css("-o-transform")      ||
		obj.css("transform");
		if(matrix !== 'none') {
			var values = matrix.split('(')[1].split(')')[0].split(',');
			var a = values[0];
			var b = values[1];
			var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
		} else { var angle = 0; }

		if(angle < 0) angle +=360;
		return angle;
	}
</script>
@endpush
