@extends('admin.layouts.main',['title' => 'Create Task'])
@push('styles')
    {{-- <link rel="stylesheet" href="style.css"> --}}
    <style style="text/css">
        .content .block {
            width: 40%;
        }

        .selection .select2-selection {
            height: auto;
        }

        .select2-container--default .select2-selection--single .select2-selection__arrow {
            top: 8px;
            right: 15px;
        }

    </style>
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="mb-2 row">
                    <div class="col-sm-6">
                        <h1 class="m-0">Create Tasks</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard v1</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="block py-12 mx-auto">
                    <form action="{{ Route('admin.task.create') }}" method="POST">
                        @csrf
                        <div class="mb-3">
                            <label for="Name" class="form-label">Name</label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                        </div>
                        <div class="mb-3">
                            <label for="description" class="form-label">Description</label>
                            <input type="text" class="form-control" name="description" value="{{ old('description') }}">
                        </div>
                        <div class="mb-3">
                            <label for="user">User</label>
                            <select class="form-control selectoption" name="user">
                                @if ($users)
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                @endif;
                            </select>
                        </div>
                        <div class="mb-3 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary">Create</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        toastr.options = {
            "positionClass": "toast-top-center",
        }
        $(document).ready(function() {
            $('#myTable').DataTable();
            $('.selectoption').select2();
        });
        @if ($errors->any())
            @foreach ($errors->all() as $item)
                toastr.error('{{ $item }}', 'Thông báo!');
            @break
            @endforeach
        @endif
    </script>
@endpush
