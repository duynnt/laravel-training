@extends('admin.layouts.main',['title' => 'Task'])
@push('styles')
{{-- <link rel="stylesheet" href="style.css"> --}}
<style style="text/css">
</style>
@endpush
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="mb-2 row">
          <div class="col-sm-6">
            <h1 class="m-0">Tasks</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="py-2 text-right right-button">
            @can('create', \App\Models\Task::class)
                <a href="{{ route('admin.task.create') }}" type="button" class="btn btn-primary">Create</a>
            @endcan
        </div>
        @if ($tasks)
          <table id="myTable" class="table table-striped" style="width:100%">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tasks as $key => $task)
                    <tr>
                        @csrf
                        <td>{{ ++$key }}</td>
                        <td>{{ $task->name }}</td>
                        <td>{{ $task->description }}</td>
                        <td>
                            @can('update', $task)
                                <a href="{{ route('admin.task.edit',$task)}}" class="btn btn-primary">edit</a>
                            @endcan
                            @can('delete',$task)
                                <button class="btn btn-danger delete" data-id="{{ $task->id }}">delete</button>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @endif
       
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready( function () {
        $('#myTable').DataTable();
    }); 
    $('.delete').click( function (){
      let id = $(this).data('id');
      var element = $(this).parent().parent();
      var token = $("meta[name='csrf-token']").attr("content");
      bootbox.confirm("Bạn có chắc chắn muốn xóa?", function(result){
        if (result){
          $.ajax({
            url: "/admin/task",
            type: 'DELETE',
            data: {
              _token: token,
              id: id
            },
            success: function (response) {
              toastr.success(response['messages'], "Thông báo");
              element.remove();
            },
            error: function (response) {
              toastr.error(response['messages'], 'Thông báo!');
            }
        }); 
        }
      });
    });
</script>
@endpush