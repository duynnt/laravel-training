<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    Route::group([
        'as' => 'admin.',
        // 'middleware' => 'is_admin'
        'prefix' => 'admin'
    ],function () {
        Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');
        Route::get('/logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout']);

        // task
        Route::resource('/task', \App\Http\Controllers\Admin\TaskController::class);
        // Route::get('/task',[\App\Http\Controllers\Admin\TaskController::class, 'index'])->name('task.index');
        // Route::get('/create-task',[\App\Http\Controllers\Admin\TaskController::class, 'create'])->name('task.create');
        // Route::post('/create-task',[\App\Http\Controllers\Admin\TaskController::class, 'store'])->name('task.create');
        // Route::get('/edit-task/{id}',[\App\Http\Controllers\Admin\TaskController::class, 'edit'])->name('task.edit');
        // Route::post('/update-task',[\App\Http\Controllers\Admin\TaskController::class, 'update'])->name('task.update');
        // Route::get('/delete-task/{id}',[\App\Http\Controllers\Admin\TaskController::class, 'destroy'])->name('task.delete');

        // product
        Route::resource('/product', \App\Http\Controllers\Admin\ProductController::class);
    });
});
Route::get('/',fn() => redirect('admin/login'));
Route::get('/admin/login', function () {
    return view('admin.auth.login');
})->name('login');
Route::post('/admin/login', [\App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login');