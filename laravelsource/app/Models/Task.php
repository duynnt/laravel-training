<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;
    protected $table = 'tasks';

    /* match policy to use */
    // protected $policies = [
    //     User::class => App\Policies\ProjectPolicy::class,
    // ];
    protected $fillable = [
        'name',
        'description',
        'user_id'
    ];
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
