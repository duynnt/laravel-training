<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function colors(){
        return $this->belongsToMany(Color::class,ProductColor::class,'product_id','color_id');
    }
}
