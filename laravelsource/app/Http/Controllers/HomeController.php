<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\Product;
class HomeController extends Controller
{
    public function index(){
        if (!Gate::allows('is-admin')) {
            $data['products'] = Product::get();
            return view('admin.pages.products')->with($data);
        }
        return view('admin.dashboard');
    }
}
