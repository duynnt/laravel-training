<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['tasks'] = Task::OrderBy('id', 'DESC')->get();
        return view('admin.pages.tasks.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Task::class);
        $data['users'] = User::where('role_id',3)->get();
        return view('admin.pages.tasks.create')->with($data);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Task::class);
        $credential = $request->validate([
            'name' => ['required'],
            'description' => ['required']
        ],[
            'name.required' => 'Name không được để trống',
            'description.required' => 'Description không được để trống'
        ]);
        if (Task::create(['name' => $request->name, 'description' => $request->description, 'user_id' => $request->user])){
            return redirect()->route('admin.task.index')->with('success','Task Created Successfully!!');
        }
  
        return back()->withErrors([
            'message' => 'Thêm thất bại vui lòng kiểm tra lại',
        ])->withinput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['task'] = Task::find($id);
        $this->authorize('update', $data['task']);
        $data['users'] = User::where('role_id',3)->get();
        return view('admin.pages.tasks.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $task = Task::find($request->id);
        $this->authorize('update', $task);
        $credential = $request->validate([
            'name' => ['required'],
            'description' => ['required']
        ],[
            'name.required' => 'Name không được để trống',
            'description.required' => 'Description không được để trống'
        ]);

        
        if ($task->update(["name" => $request->name, "description" => $request->description])){
            return redirect()->route('admin.task.index')->with('success','Task Created Successfully!!'); 
        }

        return back()->withErrors([
            'message' => 'Cập nhật thất bại vui lòng kiểm tra lại',
        ])->withinput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        $this->authorize('delete', $task);
        if ($task->delete()){
            return response()->json([
                'error'    => false,
                'messages' => "Xóa thành công",
            ], 200);
        }
        return response()->json([
            'error'    => true,
            'messages' => "Có lỗi xảy ra vui lòng thử lại",
        ], 200);
        
    }
}
