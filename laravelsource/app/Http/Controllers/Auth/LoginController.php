<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //
    public function login(Request $request){
        $credential = $request->validate([
            'email' => ['required','email'],
            'password' => ['required']
        ],[
            'email.required' => 'Email không được để trống',
            'email.email' => 'Email không đúng định dạng',
            'password.required' => 'Password không được để trống'
        ]);
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $request->session()->regenerate();
            return redirect()->intended('admin')->withSuccess('Đăng nhập thành công.');
        }
        return back()->withErrors([
            'message' => 'Thông tin đăng nhập chưa đúng vui lòng kiểm tra lại',
        ])->withinput();
    }
    public function logout(){
        Auth::logout();
        session()->flush();
        return redirect()->route('login');
    }
}
