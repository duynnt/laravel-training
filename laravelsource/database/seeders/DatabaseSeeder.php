<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Product;
use App\Models\Category;
use App\Models\ProductCategory;
use App\Models\ProductColor;
use App\Models\Color;
use App\Models\Task;
use App\Models\Role;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Product::factory(10000)->create();
        Category::factory(10)->create();
        ProductCategory::factory(10)->create();
        ProductColor::factory(1000)->create();
        Color::factory(100)->create();
        User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('admin'),
            'remember_token' => Str::random(10),
            'is_admin' => 1,
            'role_id' => 1
        ]);
        User::create([
            'name' => 'manager',
            'email' => 'manager@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('manager'),
            'remember_token' => Str::random(10),
            'is_admin' => 0,
            'role_id' => 2
        ]);
        User::create([
            'name' => 'Customer',
            'email' => 'customer@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('customer'),
            'remember_token' => Str::random(10),
            'is_admin' => 0,
            'role_id' => 3
        ]);
        Task::factory(10)->create();
        Role::Create([
            'name' => 'Administrator',
        ]);
        Role::Create([
            'name' => 'Manager',
        ]);
        Role::Create([
            'name' => 'Customer',
        ]);
    }
}
