var gulp = require("gulp");
var sass = require("gulp-sass")(require('sass'));
var rename = require("gulp-rename");

function css() {
    return gulp.src('scss/style.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(rename(function (path) {
            path.basename = "style";
            path.extname = ".min.css";
        }))
        .pipe(gulp.dest('css/'));
}

function watch() {
    // browserSync.init({
    //     server: {
    //         baseDir: "./",
    //         directory: true
    //     }
    // });
    css();
    gulp.watch('scss/style.scss', css);
    // gulp.watch('index.html').on('change', browserSync.reload);
}

gulp.task('default', watch);